```
git clone --recurse-submodules https://github.com/durandv-learn/rasa-bots.git

git submodule add https://github.com/RasaHQ/starter-pack-rasa-stack.git starter-pack-rasa-stack
git submodule update --remote --init starter-pack-rasa-stack/
```

# RasaHQ
* https://github.com/RasaHQ/starter-pack-rasa-stack.git
* https://github.com/RasaHQ/rasa_core.git
* https://github.com/RasaHQ/rasa-demo.git
* https://github.com/RasaHQ/rasa-nlu-trainer.git
* https://github.com/RasaHQ/rasa-workshop-pydata-nyc.git

# Otros repos
* https://github.com/rahul051296/small-talk-rasa-stack.git
* https://github.com/jackdh/RasaTalk/

